const express = require("express");
const router = express.Router();
const ChecarADN = require("../helpers/mutantes");
const checarADN = new ChecarADN();
const Operators = require("../helpers/operators");
const operators = new Operators();

const MutationsRepository = require("../repository/mutations.repository");
const _mutationsRepository = new MutationsRepository();
const pattern = /^[A|T|C|G|a|t|g|c|,| ]*$/;
//API REST
router.get("/", (req, res) => {
  res.send("Martin Lugo API");
});

//Consulta si es mutación y se guarda en base de datos en caso de que no exista en ella
router.post("/mutation", (req, res) => {
  const dna = req.body.dna;

  dna.map((x) => {
    if (!pattern.test(x)) {
      res
        .status(500)
        .send("Se encuentran caracteres no permitidos en la cadena");
    }
  });

  if (checarADN.hasMutation(dna)) {
    _mutationsRepository.checarSiExisteEnBd(
      dna.toString().replace(/\s/g, ""),
      true,
      function (resultado) {
        res.status(200).send();
      }
    );
  } else {
    _mutationsRepository.checarSiExisteEnBd(
      dna.toString().replace(/\s/g, ""),
      false,
      function (resultado) {
        res.status(403).send();
      }
    );
  }
});

//Obtiene una lista con los ultimos 10
router.get("/list", (req, res) => {
  _mutationsRepository.getList(function (resultado) {
    res.send(resultado);
  });
});

//Se obtienen las stats
router.get("/stats", (req, res) => {
  var ratio = 0;
  _mutationsRepository.getAllByParam(true, function (result) {
    withMutation = result[0].count;
    _mutationsRepository.getAllByParam(false, function (resultWithout) {
      withoutMutation = resultWithout[0].count;

      if (withoutMutation != 0) {
        ratio = Number(withMutation) / Number(withoutMutation);
      }
      res.json({
        count_mutations: withMutation,
        count_no_mutation: withoutMutation,
        ratio: ratio,
      });
    });
  });
});

module.exports = router;
