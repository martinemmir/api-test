//- MYSQL Module
try {
  var mysql_npm = require("../node_modules/mysql");
} catch (err) {
  console.log(
    "Cannot find `mysql` module. Is it installed ? Try `npm install mysql` or `npm install`."
  );
}

//Configuración de conexión
var db_config = {
  host: "us-cdbr-east-04.cleardb.com",
  user: "b9fdaa90cd5ecb",
  password: "9b1082c5",
  database: "heroku_a5f492ebac772f1",
};

// Variable de conexión
var connection = mysql_npm.createPool(db_config);

//Establecemos una nueva conexión
connection.getConnection(function (err) {
  if (err) {
    console.log(
      "\n\t *** Cannot establish a connection with the database. ***"
    );

    connection = reconnect(connection);
  } else {
    console.log("\n\t *** New connection established with the database. ***");
  }
});

//función de reconexión
function reconnect(connection) {
  console.log("\n New connection tentative...");

  //Se crea una nueva
  connection = mysql_npm.createPool(db_config);

  //Intentamos reconectar
  connection.getConnection(function (err) {
    if (err) {
      setTimeout(reconnect(connection), 2000);
    } else {
      console.log("\n\t *** New connection established with the database. ***");
      return connection;
    }
  });
}

//Error listener
connection.on("error", function (err) {
  if (err.code === "PROTOCOL_CONNECTION_LOST") {
    console.log(
      "/!\\ Cannot establish a connection with the database. /!\\ (" +
        err.code +
        ")"
    );
    return reconnect(connection);
  } else if (err.code === "PROTOCOL_ENQUEUE_AFTER_QUIT") {
    console.log(
      "/!\\ Cannot establish a connection with the database. /!\\ (" +
        err.code +
        ")"
    );
    return reconnect(connection);
  } else if (err.code === "PROTOCOL_ENQUEUE_AFTER_FATAL_ERROR") {
    console.log(
      "/!\\ Cannot establish a connection with the database. /!\\ (" +
        err.code +
        ")"
    );
    return reconnect(connection);
  } else if (err.code === "PROTOCOL_ENQUEUE_HANDSHAKE_TWICE") {
    console.log(
      "/!\\ Cannot establish a connection with the database. /!\\ (" +
        err.code +
        ")"
    );
  } else {
    console.log(
      "/!\\ Cannot establish a connection with the database. /!\\ (" +
        err.code +
        ")"
    );
    return reconnect(connection);
  }
});

module.exports = connection;
