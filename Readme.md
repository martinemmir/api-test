Para correr el proyecto es necesario instalar los Node Modules, para ello abrimos una nueva terminal en nuestro editor de codigo preferido y ponemos el comando "npm install" o "npm i" (sin las comillas) y damos click en enter.

Una vez instalados el proyecto se ejecuta mediante el comando "npm start"

Para hacer peticiones al servicio necesitamos una herramienta para hacer peticiones a API's como Postman. En ella haremos peticiones al "localhost:5000"

Ejemplos de peticiones:

1- Para traer los stats, nuestra llamada va a ser de tipo GET a la siguiente ruta "localhost:5000/stats" y se nos mostrara un JSON con la información

2- Para verificar si es mutacion haremos una llamada de Tipo POST a la siguiente ruta "localhost:5000/mutation", el body tiene que ser de tipo JSON, este es un ejemplo del body ""dna":["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]", en caso de ser mutación, se regresara un status 200, en caso de no ser mutación, se regresara un status 403.

3- Para obtener la lista de los ultimos 10 ADN guardados hacemos una llamada de tipo GET a la siguiente ruta "localhost:5000/list" y se nos mostrara una lista con los 10 ultimos ADN guardados y verificados

------------------------------------------------------------------------------------------------------------------

La API esta alojada en la siguiente ruta:
https://api-example-martin.herokuapp.com

y al igual que el local podemos hacer llamadas a la rutas /stats /mutation y /list
ejemplo
https://api-example-martin.herokuapp.com/stats