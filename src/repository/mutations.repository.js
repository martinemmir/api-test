//Conexion a MySQL
const mysqlConnection = require("../database");

class MutationsRepository {
  //Verifica si el dna existe en la base de datos
  checarSiExisteEnBd(string, isMutation, callback) {
    mysqlConnection.query(
      "SELECT * FROM mutations WHERE dna = " + "'" + string + "'",
      (err, rows, fields) => {
        if (!err) {
          if (rows == 0) {
            this.registrarEnBaseDeDatos(string, isMutation, function (result) {
              if (result != "error") {
                return callback(true);
              } else {
                return callback("error");
              }
            });
          } else {
            return callback(false);
          }
        } else {
          return callback("error");
        }
      }
    );
  }

  //Registra el dna en  base de datos
  registrarEnBaseDeDatos(mutation, isMutation, callback) {
    mysqlConnection.query(
      "CALL SP_CREATE_MUTATION(?,?,?)",
      [mutation, isMutation, new Date()],
      (err, rows, fields) => {
        if (!err) {
          return callback("guardado");
        } else {
          console.log(err);
          return callback("error");
        }
      }
    );
  }

  getList(callback) {
    mysqlConnection.query("CALL SP_GET_LAST_ROWS()", (err, rows, fields) => {
      if (!err) {
        return callback(rows[0]);
      } else {
        console.log(err);
        return callback("error");
      }
    });
  }
  //Obtiene los dna guardados en la base de datos dependiendo de los parametros
  getAllByParam(isMutation, callback) {
    mysqlConnection.query(
      "CALL SP_GET_MUTATIONS(?)",
      [isMutation],
      (err, rows, fields) => {
        if (!err) {
          return callback(rows[0]);
        } else {
          console.log(err);
          return callback("error");
        }
      }
    );
  }
}

module.exports = MutationsRepository;
